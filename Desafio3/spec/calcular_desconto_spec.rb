require 'calcula_desconto'
describe CalcularDesconto do

  before do
    subject = CalcularDesconto.new
  end

  describe 'calcular_desconto' do

    it "Compra R$ 100 e abaixo de R$ 200 => 10%" do
       subject.find_numbers(150, 1)
    end
    it "Compra R$ 100 e abaixo de R$ 200 => 10% mais 5%" do
      subject.find_numbers(150, 3)
    end
    it "Compra R$ 200 e abaixo de R$ 300 => 20%" do
      subject.find_numbers(250, 2)
    end
    it "Compra R$ 200 e abaixo de R$ 300 => 20% mais 5%"  do
      subject.find_numbers(250, 4)
    end
    it "Compra R$ 300 e abaixo de R$ 400 => 25%" do
      subject.find_numbers(350, 2)
    end
    it "Compra R$ 300 e abaixo de R$ 400 => 25% mais 5%" do
      subject.find_numbers(350, 3)
    end
    it "Compra Acima de R$ 400 => 30%" do
      subject.find_numbers(900, 3)
    end
  end
end
