class FilmeSearch
    include HTTParty
    #debug_output $stderr
    base_uri 'http://www.omdbapi.com'

    def initialize(body, headers)
        @options = {:body => body, :headers => headers}
    end
    def getFilme (id_film, api_key)
        self.class.get("/?i=#{id_film}&apikey=#{api_key}", @options)
    end
end