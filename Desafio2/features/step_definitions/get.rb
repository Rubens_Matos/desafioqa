Dado("que eu faça uma requisicao GET no endpoint de um filme") do
    $get_filme = HTTParty.get 'http://www.omdbapi.com/?i=tt1285016&apikey=52ec71bf'
end
  
Entao("é retornado o Titulo o ano e o idioma") do
    puts "Título....: #{$get_filme["Title"]}"
    puts "Ano.......: #{$get_filme["Year"]}"
    puts "Idioma....: #{$get_filme["Language"]}"
    expect($get_filme["Title"]).to eq 'The Social Network'
    expect($get_filme["Year"]).to eq '2010'
    expect($get_filme["Language"]).to eq 'English, French'
end