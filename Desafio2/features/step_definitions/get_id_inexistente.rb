Dado("que eu faça uma requisicao GET no endpoint de um filme inexistente") do
    $get_filme = HTTParty.get 'http://www.omdbapi.com/?i=tt12&apikey=52ec71bf'
end
  
Entao("é retornado mensagem de erro") do
    puts "Mensagem....: #{$get_filme["Error"]}"
    expect($get_filme["Error"]).to eq 'Incorrect IMDb ID.'
end