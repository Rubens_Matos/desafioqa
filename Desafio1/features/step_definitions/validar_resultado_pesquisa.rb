Quando("estou na página {int} da pesquisa") do |pagina|
    sleep 3
    page.execute_script "window.scrollBy(0,10000)"
    click_link pagina
end
  
Entao("o sistema NÃO exibe a lista mádicos de São Paulo") do
    expect(page).to have_no_content "São Paulo"
end