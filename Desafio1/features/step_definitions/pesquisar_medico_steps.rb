Dado("que eu acesse a página inicial da Unimed") do
    visit "/"
    @pesquisaMedico = PesquisaMedico.new
end
  
Quando("eu clicar na opção de menu Guia Médico") do
    click_link('Guia Médico')
end
  
Quando("informar os campos de pesquisa de médico") do
    @pesquisaMedico.cmpPesquisa.set 'Ortopedia'
    @pesquisaMedico.btnPesquisar.click
end

Quando("informar o estado {string} e a cidade {string}") do |estado, cidade|
    @pesquisaMedico.cmpLocalidade.first.click
    execute_script('window.localStorage.clear()')
    sleep 5
    @pesquisaMedico.selecionarEstado.click
    sleep 3
    execute_script('window.localStorage.clear()')
    @pesquisaMedico.cmpLocalidade.last.click
    @pesquisaMedico.selecionarCidade.click
    choose('UNIMED RIO')
    click_button "Continuar"
end

Entao("o sistema exibe a lista com os nomes de acordo filtros informados") do
     expect(page).to have_content("Ortopedia")
     expect(page).to have_content("Rio de Janeiro")
end