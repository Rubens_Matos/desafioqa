#language: pt
Funcionalidade: Validar pesquisa de médicos no Rio de Janeiro

Eu, como usuário da Unimed 
desejo realizar uma pesquisa de
médicos do estado do Rio de Janeiro.

Contexto:
      Dado que eu acesse a página inicial da Unimed
      Quando eu clicar na opção de menu Guia Médico
      E informar os campos de pesquisa de médico
      E informar o estado "Rio de Janeiro" e a cidade "Rio de Janeiro"
@hooks_pesquisa
Cenário: Relizar pesquisa de mádico e especialidade no Rio de Janeiro
      Entao o sistema exibe a lista com os nomes de acordo filtros informados

@validar_pesquisa
Esquema do Cenário: Validar se a pesquisa NÃO apresenta resultados com cidade São Paulo 
      E estou na página <numero> da pesquisa
      Entao o sistema NÃO exibe a lista mádicos de São Paulo
Exemplos:
    | numero |
    |  1     |
    |  2     |
    |  3     |