# Classe para mapear os elementos da pagina home
class PesquisaMedico < SitePrism::Page
  element :cmpPesquisa, '#campo_pesquisa'
  elements :cmpLocalidade, :css, '.css-k71zgk'
  element :selecionarEstado, '#react-select-2-option-18'
  element :selecionarCidade, '#react-select-3-option-67'
  elements :especialidade, '#txt_especialidade'
  elements :cidade_endereco, '#txt_endereco'
  elements :paginacao, :css, ".span12.pagination.text-center.no-margin-left"
  element :btnPesquisar, '#btn_pesquisar'
  element :limpar_campo, :css, 'css-t2flpr-control'

  def selecionar_estado(estado)
    selecionarEstado.each do |g|
      puts g.text
      if g.text == estado
        g.click
        break
      elsif g.text != estado
        select "Rio de Janeiro"
      end
    end
  end
end